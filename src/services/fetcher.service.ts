import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class FetcherService {
  private baseUrl = 'http://localhost:5000/';

  constructor(private http: HttpClient) {
  }

  public async get(url: string): Promise<any> {
    return await this.http.get(this.baseUrl + url).toPromise();
  }

  public async post(url: string, data: any): Promise<any> {
    return await this.http.post(this.baseUrl + url, data).toPromise();
  }
}
