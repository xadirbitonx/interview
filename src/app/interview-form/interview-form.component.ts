import {Component, Input, OnInit} from '@angular/core';
import {FormArray, FormBuilder, FormGroup} from '@angular/forms';

@Component({
  selector: 'app-interview-form',
  templateUrl: './interview-form.component.html',
  styleUrls: ['./interview-form.component.less']
})
export class InterviewFormComponent implements OnInit {
  @Input() pastInterview: { [key: string]: any; } | undefined;
  public interviewForm: FormGroup;
  private reader = new FileReader();
  public professionalScoreText = ProfessionalScoreText;
  public scoreText = ScoreText;
  public overallScoreText = OverallScoreText;

  constructor(private formBuilder: FormBuilder) {
    this.interviewForm = this.initForm();
    this.reader.onloadend = () => {
      this.interviewForm.get('image')?.setValue(this.reader.result);
    };
  }

  ngOnInit(): void {
    this.interviewForm = this.initForm();
  }

  private initForm(): FormGroup {
    let newForm: any = {
      name: '',
      TZ: null,
      interviewDate: new Date(),
      profile: null,
      address: '',
      age: null,
      imagePath: '',
      image: null,
      interviewers: this.formBuilder.array([this.formBuilder.control('')]),
      preInterview: '',
      personalBackground: '',
      highschoolName: '',
      megama: '',
      highschoolNotes: '',
      highschoolNotes2: '',
      goodTraits: '',
      badTraits: '',
      creativity: '',
      motivation: '',
      difficulties: '',
      successExample: '',
      failureExample: '',
      project: '',
      theoryQuestion1: '',
      theoryQuestion2: '',
      theoryAnswer1: '',
      theoryAnswer2: '',
      theoryScore1: null,
      theoryScore2: null,
      practicalQuestion1: '',
      practicalQuestion2: '',
      practicalAnswer1: '',
      practicalAnswer2: '',
      practicalScore1: null,
      practicalScore2: null,
      logicalQuestionLevel1: null,
      logicalQuestionLevel2: null,
      logicalQuestion1: '',
      logicalQuestion2: '',
      logicalAnswer1: '',
      logicalAnswer2: '',
      logicalScore1: null,
      logicalScore2: null,
      oralExpression: '',
      freeText: '',
      theoryOverallScore: '',
      practicalOverallScore: '',
      technologicalOrientationScore: '',
      professionalPotentialScore: '',
      criticalThinkingScore: '',
      overallProfessionalScore: '',
      peopleSkillsScore: '',
      characteristicsScore: '',
      motivationScore: '',
      overallCharacterScore: '',
      overAllScore: ''
    };
    if (this.pastInterview) {
      const controls = this.pastInterview.interviewers.map((interviewer: any) => {
        return this.formBuilder.control(interviewer);
      });
      newForm = Object.assign({}, this.pastInterview);
      newForm.interviewers = this.formBuilder.array(controls);
      newForm.imagePath = '';
    }
    return this.formBuilder.group(newForm);
  }

  public onFileChange(event: any): void {
    const imageFile = event.target.files[0];
    if (imageFile) {
      this.reader.readAsDataURL(imageFile);
    }
  }

  public get interviewers(): FormArray {
    return this.interviewForm.get('interviewers') as FormArray;
  }

  public get image(): string {
    return this.interviewForm.get('image')?.value;
  }

  public addInterviewer(): void {
    this.interviewers.push(this.formBuilder.control(''));
  }

  public removeInterviewer(index: number): void {
    this.interviewers.removeAt(index);
  }

  public exportInterview(): void {
    const a = document.createElement('a');
    const file = new Blob([JSON.stringify(this.interviewForm.value)], {type: 'json'});
    a.href = URL.createObjectURL(file);
    a.download = this.interviewForm.get('name')?.value ? this.interviewForm.get('name')?.value + '.json' : 'noname.json';
    a.click();
  }
}

export enum ProfessionalScoreText {
  s1 = '1 - לא עובר סף',
  s2 = '2 - ידע בסיסי בלבד',
  s3 = '3 - קיים ידע. בעל פוטנציאל',
  s4 = '4 - ידע מקצועי ממוצע',
  s5 = '5 - חזק מאוד, בעל ידע אקדמאי'
}

export enum ScoreText {
  s1 = '1 - לא עובר סף',
  s2 = '2 - בסיסי בלבד',
  s3 = '3 - ממוצע',
  s4 = '4 - גבוה',
  s5 = '5 - גבוה מאוד'
}

export enum OverallScoreText {
  s1 = '1 - אינו מתאים',
  s2 = '2 - התאמתו מוטלת בספק. סיכויים נמוכים להצליח',
  s3 = '3 - אין התנגדות לשלבו ביחידה, אם אין טובים ממנו סיכויים בינוניים להצליח',
  s4 = '4 - אפשר לשלבו ביחידה. סיכויים סבירים להצליח',
  s5 = '5 - כדאי לקבלו. בעל סיכויים טובים להצליח'
}


