import {Component} from '@angular/core';
import {InterviewService} from '../../services/interview.service';
import {FetcherService} from '../../services/fetcher.service';
import {IInterview} from '../app.component';

@Component({
  selector: 'app-interviews-list',
  templateUrl: './interviews-list.component.html',
  styleUrls: ['./interviews-list.component.less']
})
export class InterviewsListComponent {
  public searchText = '';
  public requestStatus: RequestStatus = RequestStatus.loading;
  public filteredInterviewsList: IInterview[] = [];
  private interviewsList: IInterview[] = [];
  constructor(private interviewService: InterviewService, private fetcher: FetcherService) {
    this.getInterviews();
  }

  public async getInterviews(): Promise<void> {
    this.requestStatus = RequestStatus.loading;
    try {
      this.interviewsList = await this.fetcher.get('getInterviews');
      this.requestStatus = RequestStatus.success;
    } catch (e) {
      this.requestStatus = RequestStatus.fail;
      console.log(e);
    }
  }

  public filterInterviews(): void {
    if (this.searchText !== '') {
      this.filteredInterviewsList = this.interviewsList.filter((interview: any) => {
        return interview.name.includes(this.searchText);
      });
    } else {
      this.filteredInterviewsList = [];
    }
  }

  public showPastInterview(interview: IInterview): void {
    this.interviewService.selectedInterview = interview;
  }

  public getInterviewDate(interview: IInterview): string {
    return new Date(interview.interviewDate).toLocaleDateString();
  }
}

enum RequestStatus {
  loading,
  success,
  fail
}
