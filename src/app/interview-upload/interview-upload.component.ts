import {Component} from '@angular/core';
import {FetcherService} from '../../services/fetcher.service';

@Component({
  selector: 'app-interview-upload',
  templateUrl: './interview-upload.component.html',
  styleUrls: ['./interview-upload.component.less']
})
export class InterviewUploadComponent {
  public interviewFile: any = null;

  constructor(private fetcher: FetcherService) {
  }

  public async uploadInterview(): Promise<void> {
    if (this.interviewFile) {
      try {
        const formData: FormData = new FormData();
        formData.append('file', this.interviewFile[0], this.interviewFile[0].name);
        await this.fetcher.post('saveInterview', formData);
        this.resetPage();
        alert('ראיון הועלה בהצלחה!');
      } catch (e) {
        console.log(e);
        alert('העלת ראיון נכשלה, נסה שוב מאוחר יותר');
      }
    }
  }

  public onFileChange(event: any): void {
    this.interviewFile = event.target.files;
  }

  private resetPage(): void {
    const fileInput: any = document.getElementById('file');
    fileInput.value = '';
    this.interviewFile = null;
  }
}
