import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.less']
})
export class AppComponent {
}

export interface IInterview {
  name: '';
  TZ: number;
  interviewDate: Date;
  profile: number;
  address: '';
  age: number;
  imagePath: '';
  image: '';
  interviewers: string[];
  preInterview: '';
  personalBackground: '';
  highschoolName: '';
  megama: '';
  highschoolNotes: '';
  highschoolNotes2: '';
  goodTraits: '';
  badTraits: '';
  creativity: '';
  motivation: '';
  difficulties: '';
  successExample: '';
  failureExample: '';
  project: '';
  theoryQuestion1: '';
  theoryQuestion2: '';
  theoryAnswer1: '';
  theoryAnswer2: '';
  theoryScore1: number;
  theoryScore2: number;
  practicalQuestion1: '';
  practicalQuestion2: '';
  practicalAnswer1: '';
  practicalAnswer2: '';
  practicalScore1: number;
  practicalScore2: number;
  logicalQuestionLevel1: number;
  logicalQuestionLevel2: number;
  logicalQuestion1: '';
  logicalQuestion2: '';
  logicalAnswer1: '';
  logicalAnswer2: '';
  logicalScore1: number;
  logicalScore2: number;
  oralExpression: '';
  freeText: '';
  theoryOverallScore: '';
  practicalOverallScore: '';
  technologicalOrientationScore: '';
  professionalPotentialScore: '';
  criticalThinkingScore: '';
  overallProfessionalScore: '';
  peopleSkillsScore: '';
  characteristicsScore: '';
  motivationScore: '';
  overallCharacterScore: '';
  overAllScore: '';
}
