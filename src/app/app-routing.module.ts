import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {InterviewFormComponent} from './interview-form/interview-form.component';
import {InterviewsListComponent} from './interviews-list/interviews-list.component';
import {InterviewDisplayComponent} from './interview-display/interview-display.component';
import {InterviewUploadComponent} from './interview-upload/interview-upload.component';

const routes: Routes = [
  {path: '', redirectTo: 'newInterview', pathMatch: 'full'},
  {path: 'newInterview', component: InterviewFormComponent},
  {path: 'allInterviews', component: InterviewsListComponent},
  {path: 'displayInterview', component: InterviewDisplayComponent},
  {path: 'uploadInterview', component: InterviewUploadComponent}
];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
