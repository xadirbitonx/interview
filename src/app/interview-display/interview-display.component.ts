import { Component, OnInit } from '@angular/core';
import {InterviewService} from '../../services/interview.service';

@Component({
  selector: 'app-interview-display',
  templateUrl: './interview-display.component.html',
  styleUrls: ['./interview-display.component.less']
})
export class InterviewDisplayComponent implements OnInit {
  public selectedInterview: any;
  constructor(private interviewService: InterviewService) {
    this.selectedInterview = interviewService.selectedInterview;
    const wow = 5;
  }

  ngOnInit(): void {
  }

}
