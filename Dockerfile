FROM node:14.15.1 as build

WORKDIR /app

COPY . .
RUN npm install
RUN npm run build

FROM nginx:latest

COPY --from=build /app/dist/interview /usr/share/nginx/html

EXPOSE 80
